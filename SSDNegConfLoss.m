% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期六 01/10/2016 19:09.
% Last Revision: 星期四 12/01/2017 14:00.
%qt

function scores = SSDNegConfLoss(conf_data, neg_idx)
    
    conf_data_now = SoftMax(conf_data); % 计算分类结果的softmax        
    scores = -log(conf_data_now(1, neg_idx));
    
end
