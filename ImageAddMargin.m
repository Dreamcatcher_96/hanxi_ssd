% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期日 19/03/2017 00:28.
% Last Revision: 星期日 19/03/2017 00:28.

function [img_large, offset] = ImageAddMargin(img, m_ratio)

    [img_h, img_w, img_c] = size(img);

    large_h = ceil((1 + 2 * m_ratio) * img_h);
    large_w = ceil((1 + 2 * m_ratio) * img_w);

    img_large = ones(large_h * large_w, img_c);
    if img_c > 1
        channel_mean = mean(reshape(img, img_h * img_w, img_c), 1);
        img_large = bsxfun(@times, img_large, channel_mean);
        img_large = reshape(img_large, large_h, large_w, img_c);
    else
        img_large = img_large .* mean(img(:));
    end

    offset = [floor(img_h * m_ratio), floor(img_w * m_ratio)];
    img_large(offset(1) + 1 : offset(1) + img_h, ...
         offset(2) + 1 : offset(2) + img_w, :) = img;

end



