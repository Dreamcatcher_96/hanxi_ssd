% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期日 19/03/2017 11:14.
% Last Revision: 星期日 19/03/2017 11:14.

function [im, obj_bbox, obj_class] = ReadDetDataset(im_data)

%     im_name = [data_opt.image_root, '/', im_data.filename];
    im_name = im_data.filename;
    im = imread(im_name);
    obj = im_data.object;
%     im_siz = size(im);

    num_obj = numel(obj);
    obj_bbox = zeros(4, num_obj);
    obj_class = zeros(1, num_obj);
    for i_bbox = 1 : num_obj
        bbox_now = obj(i_bbox).bndbox;
        obj_bbox(:, i_bbox) = bbox_now';
        obj_class(i_bbox) = obj(i_bbox).class_id;
    end

end



