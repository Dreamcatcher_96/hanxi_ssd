% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期六 18/03/2017 15:34.
% Last Revision: 星期六 18/03/2017 15:34.

function ChangeSolverFile(solver_file, train_file, snapshot_path)

    if nargin == 2
        snapshot_path = './';
    end

    train_file = regexprep(train_file, '\/', '\\\/');
    sed_command = sprintf('sed -i ''s/train_net: ".*"/train_net: "%s"/g\'' %s', ...
                                            train_file, solver_file);
    system(sed_command);

    snapshot_file = regexprep(snapshot_path, '\/', '\\\/');
    sed_command = sprintf('sed -i ''s/snapshot_prefix: ".*"/snapshot_prefix: "%s"/g\'' %s', ...
                                            snapshot_file, solver_file);
    system(sed_command);

end



