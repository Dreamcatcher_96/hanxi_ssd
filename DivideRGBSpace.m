% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期日 12/03/2017 15:41.
% Last Revision: 星期日 12/03/2017 15:41.

function rgb_array = DivideRGBSpace(n)

    n_level = ceil(n^0.3333);
    [I, J, K] = ind2sub([n_level, n_level, n_level], 1 : n);
    R = min(1, I / n_level);
    G = min(1, J / n_level);
    B = min(1, K / n_level);

    rgb_array = 255 * [R(:), G(:), B(:)];

end



