% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期三 11/01/2017 16:35.
% Last Revision: 星期三 11/01/2017 16:35.
%qt

function [caffe_data, caffe_label] = GetCaffeInput(img, ...
                                obj_bbox, obj_class, net_opt)

    caffe_data = CaffeInputPreProcess(img, ...
            net_opt.mean_value, 'VGG', net_opt.data_size);

    if nargout == 2
        caffe_label = SSDMakeObjectLabel(obj_bbox, obj_class, img);
    end

end

