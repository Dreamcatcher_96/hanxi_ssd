% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期六 01/10/2016 19:09.
% Last Revision: 星期三 15/03/2017 14:26.
%qt

function SSD_Net_Train_GPU2()

    clc;
    clear;
    close all;
    dbstop if error;
    RandStream.setGlobalStream(RandStream('mt19937ar', 'Seed', 'shuffle'));
    addpath('./nms/');
    addpath('./dataArgu/');

    %% Initialization
    % caffe reset
    gpu_id = 2;
    use_gpu = true;
    CaffeRestart(gpu_id, use_gpu);

    % options for the network and dataset
%     job_name = 'hanxi_ssd_imgaug_finetune';
    job_name = 'hanxi_ssd_goodAug120000';
    net_opt = SSDNetOption();
%     net_opt.solver_file = './models/SSD_300x300_Ori/solver1.prototxt';
%     net_opt.init_file = './models/SSD_300x300_Ori/VGG_VOC0712_SSD_300x300_iter_60000.caffemodel';
%     net_opt.init_file = './models/finetuned_ssd/CaffeModel_GPU1_DatasetVOC0712_TrnTstR80_TrnValR00_RandSeed1982/final_net_iter_30000.caffemodel';
%     net_opt.train_file = './models/SSD_300x300_Ori/train.prototxt';
    net_opt.solver_file = './models/Ori_VGG16/solver.prototxt';
    net_opt.init_file = './models/Ori_VGG16/VGG_ILSVRC_16_layers_fc_reduced.caffemodel';
%     net_opt.init_file = './models/hanxi_ssd/CaffeModel_GPU2_DatasetVOC0712_TrnTstR80_TrnValR00_RandSeed1982/final_net_iter_60000.caffemodel';
%     net_opt.init_file = './models/hanxi_ssd_imgaug_finetune/CaffeModel_GPU1_DatasetVOC0712_TrnTstR80_TrnValR00_RandSeed1982/final_net_iter_60000.caffemodel';
    net_opt.train_file = './models/Ori_VGG16/train.prototxt';

    % some changes for fine-tuning
    net_opt.pos_overlap = 0.5;
    net_opt.neg_overlap = 0.5;
    net_opt.neg_pos_ratio = 3;
    net_opt.max_iter = 120000;
    net_opt.step_size = 80000;

    ChangeSolverFile(net_opt.solver_file, net_opt.train_file);
    dataset_name = 'voc0712';
    data_opt = DatasetOption(dataset_name);
    data_opt.gen_channel_mean = net_opt.mean_value;

    % the random initialization
    rand_seed = 1982;
    rng(rand_seed);

    % the caching and save path
    sub_folder_name = ['CaffeModel_GPU', num2str(gpu_id), '_Dataset', ...
        upper(dataset_name), '_RandSeed', num2str(rand_seed), '_MaxIter', ...
            num2str(net_opt.max_iter), '_StepSiz', num2str(net_opt.step_size), ...
                '_PosOverlap', num2str(net_opt.pos_overlap), '_NegOverlap', num2str(net_opt.neg_overlap), ...
                                '_NegPosR', num2str(net_opt.neg_pos_ratio)];
    cache_path = ['./train_cache/', job_name, '/', sub_folder_name];
    save_path = ['./models/', job_name, '/', sub_folder_name];
    MakeDirIfMissing(cache_path);
    MakeDirIfMissing([cache_path, '/log/']);
    MakeDirIfMissing(save_path);
    system('tar -cvvf mfiles.tar *.m');
    system(['mv mfiles.tar ', save_path]);

    % whethe show the inter-media results of training
    show_pause = 0;
    show_interval = 1000;
    tst_siz = 1000;

    % network initialization
    caffe_solver = caffe.Solver(net_opt.solver_file);
    caffe_solver.net.copy_from(net_opt.init_file);

    % obtain the trianing, validation and test sets.
    dataset_path = '/media/hanx/Back_All/VOCDataset/';
    dataset_year = 'voc0712';
    dataset_usage = 'trainval';
    dataset_trn = GetDatasetNew(dataset_path, dataset_year, dataset_usage);

    dataset_path = '/media/hanx/Back_All/VOCDataset/VOCdevkit2007/';
    dataset_year = 'voc2007';
    dataset_usage = 'test';
    dataset_tst = GetDatasetNew(dataset_path, dataset_year, dataset_usage);
    num_tst_data = numel(dataset_tst);
    rand_tst_idx = randperm(num_tst_data, tst_siz);
    dataset_tst = dataset_tst(rand_tst_idx);

    all_class_name = {'aero', 'bike', 'bird', 'boat', 'bottle', 'bus', 'car', ...
        'cat', 'chair', 'cow', 'table', 'dog', 'horse', 'mbike', 'person', 'plant', ...
                                        'sheep', 'sofa', 'train', 'tv'};
    all_class_color = DivideRGBSpace(length(all_class_name));

    % log file initialization
    timestamp = datestr(datevec(now()), 'yyyymmdd_HHMMSS');
    log_file = [cache_path, '/log/train_', timestamp, '.txt'];
    diary(log_file);

    % display the net and dataset infomation
    disp('net config:');
    disp(net_opt);
    disp('dataset config:');
    disp(data_opt);

    %% feed-forward and back-propogation
    loss_record = zeros(1, net_opt.max_iter);
    ap_record = zeros(1, net_opt.max_iter);
    num_trn_data = numel(dataset_trn);

    N = net_opt.max_iter * net_opt.batch_size;
    batch_idx_all = randi(num_trn_data, [1, N]);
    batch_idx_all = reshape(batch_idx_all, net_opt.batch_size, []);
    for i_trn = 1 : net_opt.max_iter

        data_prepare_start = tic;
        % data of mini-batch
        batch_dataset = dataset_trn(batch_idx_all(:, i_trn));
        % get the sub-images
        [sub_img, subimg_obj_bbox, subimg_obj_class] = SSDImageSampler(batch_dataset, ...
                                                    net_opt.batch_sampler, data_opt);

        % get the input of the ssd
        [net_input_data, net_input_label] = GetCaffeInput(sub_img, ...
                            subimg_obj_bbox, subimg_obj_class, net_opt);
        data_prepare_time = toc(data_prepare_start);

        % forward
        forward_start = tic;
        caffe_solver.net.forward({net_input_data});
        forward_time = toc(forward_start);

        % gather postive and negative samples
        sample_start = tic;
        mbox_loc = caffe_solver.net.blobs('mbox_loc').get_data();
        mbox_conf = caffe_solver.net.blobs('mbox_conf').get_data();
        loc_pred = reshape(mbox_loc, 4, [], net_opt.batch_size);
        conf_pred = reshape(mbox_conf,net_opt.num_classes, ...
                                            [], net_opt.batch_size);
        max_pos_scores = SSDMaxScore(conf_pred);
        [pos_dbox_all, pos_gbox_all, neg_dbox_all, num_pos, num_neg] = ...
                SSDSelectPosNegSample(max_pos_scores, net_opt.dbox, net_input_label, net_opt);
        sample_time = toc(sample_start);

        loss_start = tic;
        % smooth L1 forward
        [loc_loss, conf_loss] = SSDLossForward(pos_dbox_all, ...
            pos_gbox_all, loc_pred, conf_pred, net_opt.dbox, net_input_label, ...
                                    neg_dbox_all, num_pos, num_neg, net_opt);
        % the current loss
        loss_record(i_trn) = (loc_loss.loss + conf_loss.loss) / (num_pos + num_neg);
        % smooth L1 backward
        SSDLossBackward(caffe_solver, loc_loss, conf_loss, pos_dbox_all, ...
                                            neg_dbox_all, num_pos, net_opt);
        loss_time = toc(loss_start);

        % back-propagation
        back_start = tic;
        caffe_solver.net.backward_prefilled();
        rate_now = net_opt.base_lr * ...
                net_opt.lr_gamma^(floor(i_trn / net_opt.step_size));
        caffe_solver.update(single(rate_now));
        back_time = toc(back_start);

        if show_pause > 0
            hf = figure(1);
            for i_img = 1 : length(sub_img)
                loc_pred_now = loc_pred(:, :, i_img);
                conf_pred_now = conf_pred(:, :, i_img);
                [bbox_pred_now, score_pred_now] = ...
                    DetectionInference(sub_img{i_img}, net_opt.dbox, loc_pred_now, ...
                                                    conf_pred_now, net_opt);
                ShowDetectionResults(hf, sub_img{i_img}, bbox_pred_now, ...
                    score_pred_now, subimg_obj_bbox{i_img}, subimg_obj_class{i_img}, ...
                                            all_class_name, all_class_color);
                pause(show_pause);clf;
            end
        end

        % display training state
        fprintf( '%s \t i_trn %6d / %6d  loss: %.6f\tlr:%.6f\n', ...
            datestr(now()), i_trn, net_opt.max_iter, loss_record(i_trn), rate_now);
        fprintf( 'data:%2.2f, forward:%2.2f, sample:%2.2f, loss:%2.2f, back:%2.2f, all:%2.2f\n', ...
        data_prepare_time, forward_time, sample_time, loss_time, back_time, ...
        data_prepare_time + forward_time + sample_time + loss_time + back_time);
        if ~mod(i_trn, show_interval)
           X = 1 : i_trn;
           figure(1);subplot(2, 1, 1);
           line(X, loss_record(1 : i_trn));
           title(sprintf('Training Loss, iter-%d.jpg', i_trn));
           saveas(1, [cache_path, '/training_curve.jpg']);
           class_ap = SSDTest(dataset_tst, caffe_solver.net, all_class_name, net_opt);
           v = (1 : show_interval) ./ show_interval;
           t = i_trn - show_interval;
           if t >= 1
               u = ap_record(t) + (mean(class_ap) - ap_record(t)) .* v;
           else
               u = ones(1, show_interval) * mean(class_ap);
           end
           ap_record(t + 1 : i_trn) = u;
           figure(1);subplot(2, 1, 2);
           line(X, ap_record(1 : i_trn));
           title(sprintf('Test AP = %2.2f, iter-%d.jpg', ap_record(i_trn), i_trn));
           pause(10);
        end
        if ~mod(i_trn, net_opt.save_iter)
           caffe_solver.net.save([cache_path, ...
                '/cached_net_iter_', num2str(i_trn), '.caffemodel']);
        end

    end

    caffe_solver.net.save([save_path, ...
            '/final_net_iter_', num2str(i_trn), '.caffemodel']);
    diary off;
    system(['mv ', log_file, ' ', save_path]);
    % to save the disk space
    system(['rm ', cache_path, '/cached_net_iter_*.caffemodel']);

end

