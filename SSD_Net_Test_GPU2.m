% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期六 01/10/2016 19:09.
% Last Revision: 星期四 30/03/2017 14:53.
%qt

function SSD_Net_Test_GPU2()

    clc;
    clear;
    close all;
    dbstop if error;
    RandStream.setGlobalStream(RandStream('mt19937ar', 'Seed', 'shuffle'));
    addpath('./nms/');
    addpath('./dataArgu/');

    %% Initialization
    % caffe reset
    gpu_id = 2;
    use_gpu = true;
    CaffeRestart(gpu_id, use_gpu);

    % options for the network and dataset
    job_name = 'ori_ssd';
    net_opt = SSDNetOption();
    net_opt.init_file = './models/SSD_300x300_Ori/VGG_VOC0712_SSD_300x300_iter_60000.caffemodel';
%     net_opt.init_file = './models/hanxi_ssd_imgaug/CaffeModel_GPU2_DatasetVOC0712_TrnTstR80_TrnValR00_RandSeed1982/final_net_iter_60000.caffemodel';
%     net_opt.init_file = './models/finetuned_ssd/CaffeModel_GPU1_DatasetVOC0712_TrnTstR80_TrnValR00_RandSeed1982/final_net_iter_30000.caffemodel';
%     net_opt.deploy_file = './models/SSD_300x300_Ori/train.prototxt';
%     net_opt.init_file = './models/hanxi_ssd_imgaug_finetune/CaffeModel_GPU1_DatasetVOC0712_TrnTstR80_TrnValR00_RandSeed1982/final_net_iter_60000.caffemodel';
    net_opt.deploy_file = './models/Ori_VGG16/deploy.prototxt';

    dataset_name = 'voc0712';
    data_opt = DatasetOption(dataset_name);

    % the caching and save path
    sub_folder_name = ['CaffeModel_GPU', ...
        num2str(gpu_id), '_Dataset', upper(dataset_name), '_TrnTstR', ...
            sprintf('%02d', round(100 * data_opt.R)), '_TrnValR', ...
                        sprintf('%02d', round(100 * data_opt.R_val))];
    save_path = ['./results/', job_name, '/', sub_folder_name];
    MakeDirIfMissing(save_path);
    system('tar -cvvf mfiles.tar *.m');
    system(['mv mfiles.tar ', save_path]);

    % whethe show the inter-media results of training
    show_pause = 0;

    % network initialization
    test_net = caffe.Net(net_opt.deploy_file, 'test');
    test_net.copy_from(net_opt.init_file);

    % obtain the trianing, validation and test sets.
    dataset_path = '/media/hanx/Back_All/VOCDataset/';
    dataset_year = 'voc2007';
    dataset_usage = 'test';
    dataset = GetDatasetNew(dataset_path, dataset_year, dataset_usage);
    all_class_name = {'aero', 'bike', 'bird', 'boat', 'bottle', 'bus', 'car', ...
        'cat', 'chair', 'cow', 'table', 'dog', 'horse', 'mbike', 'person', 'plant', ...
                                        'sheep', 'sofa', 'train', 'tv'};
    all_class_color = DivideRGBSpace(length(all_class_name));
    num_class = length(all_class_name);

    % display the net and dataset infomation
    disp('net config:');
    disp(net_opt);
    disp(data_opt);


    %% test
    num_tst = numel(dataset);
    n_batch = ceil(num_tst ./ net_opt.batch_size);
    u = n_batch * net_opt.batch_size - num_tst;
    batch_idx_all = reshape([1 : num_tst, randi(num_tst, 1, u)], ...
                                    net_opt.batch_size, []);
    bbox_pred = cell(num_class, num_tst);
    score_pred = cell(num_class, num_tst);
    aboxes = cell(num_class, num_tst);
    ground_truth = cell(num_class, num_tst);
    obj_difficulty = cell(num_class, num_tst);
    been_tested = false(1, num_tst);
    for i_tst = 1 : size(batch_idx_all, 2)

        fprintf('Hanxi SSD test start, batch-%d, %d images, ', ...
                                    i_tst, size(batch_idx_all, 1));
        tst_start = tic;

        % get the input of the ssd
        batch_idx = batch_idx_all(:, i_tst);
        batch_dataset = dataset(batch_idx);
        [tst_img, tst_obj_bbox, tst_obj_class, tst_obj_difficulty] = ...
                                            SSDTestImage(batch_dataset);
        net_input_data = GetCaffeInput(tst_img, ...
                    tst_obj_bbox, tst_obj_class, net_opt);

        % forward
        FlexibleForward(net_input_data, test_net);

        % detection inference
        mbox_loc = test_net.blobs('mbox_loc').get_data();
        mbox_conf = test_net.blobs('mbox_conf').get_data();
        loc_pred = reshape(mbox_loc, 4, [], net_opt.batch_size);
        conf_pred = reshape(mbox_conf, num_class + 1, [], net_opt.batch_size);

        % detection inference
        bbox_pred_batch = cell(num_class, length(tst_img));
        score_pred_batch = cell(num_class, length(tst_img));
        for i_img = 1 : length(tst_img)
            loc_pred_now = loc_pred(:, :, i_img);
            conf_pred_now = conf_pred(:, :, i_img);
            [bbox_pred_batch(:, i_img), score_pred_batch(:, i_img)] = ...
                DetectionInference(tst_img{i_img}, net_opt.dbox, loc_pred_now, ...
                                                            conf_pred_now, net_opt);
        end

        fprintf(' ... done, took %2.3f sec.\n', toc(tst_start));

        % save detection results
        bbox_pred(:, batch_idx) = bbox_pred_batch;
        score_pred(:, batch_idx) = score_pred_batch;
        for i_class = 1 : num_class
            aboxes(i_class, batch_idx) = cellfun(@(x,y) [x', y'], ...
                bbox_pred_batch(i_class, :), score_pred_batch(i_class, :), 'UniformOutput', false);
            this_class = cellfun(@(x) x == i_class, ...
                        tst_obj_class, 'UniformOutput', false);
            ground_truth(i_class, batch_idx) = ...
                cellfun(@(x,y) x(:, y)', tst_obj_bbox, this_class, 'UniformOutput', false);
            obj_difficulty(i_class, batch_idx) = cellfun(@(x,y) x(y), ...
                        tst_obj_difficulty, this_class, 'UniformOutput', false);
        end
        been_tested(batch_idx) = true;

        % show detection results
        if show_pause <= 0, continue; end

        hf = figure(1);
        for i_img = 1 : length(tst_img)
            bbox_pred_now = bbox_pred_batch(:, i_img);
            score_pred_now = score_pred_batch(:, i_img);
            ShowDetectionResults(hf, tst_img{i_img}, bbox_pred_now, ...
                score_pred_now, tst_obj_bbox{i_img}, tst_obj_class{i_img}, ...
                                        all_class_name, all_class_color);
            pause(show_pause);clf;
        end

    end

    aboxes(:, ~been_tested) = [];
    ground_truth(:, ~been_tested) = [];
    obj_difficulty(:, ~been_tested) = [];
    save([job_name, '_result.mat'], 'aboxes', ...
                        'ground_truth', 'obj_difficulty', '-v7.3');
%     load([job_name, '_result.mat']);
    ap_all = DetectionResEval(aboxes, ground_truth, ...
        obj_difficulty, all_class_name, all_class_name, net_opt.nms_overlap);
    disp(mean(ap_all));


end


