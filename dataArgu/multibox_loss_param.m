function param = loss_param(varargin)
    ip = inputParser;
    
    ip.addParamValue('loc_weight',          1,              @isscalar);
    ip.addParamValue('overlap_threshold',   0.5,            @isscalar);
    % If true, also consider difficult ground truth.
    ip.addParamValue('use_difficult_gt',    true,           @islogical);
    
    ip.addParamValue('neg_pos_ratio',       3.0,            @isscalar);
    ip.addParamValue('neg_overlap',         0.5,            @isscalar);
    
    
    ip.parse(varargin{:});
    param = ip.Results;
end