classdef BatchSampler < handle
   properties(Access = private)
       batchSamplerParam
   end
   
   methods
       function obj = BatchSampler()
            obj.batchSamplerParam = batchsampler_param();
       end
       
       function [sampledImg, BBoxes] = generateBatchSamples(obj, img, BBoxes)
            sampledBBoxes   = [];
            for ii = 1 : length(obj.batchSamplerParam)
                sourceBBox  = [0, 0, 1, 1];
                samples = obj.generateSamples(sourceBBox, BBoxes, obj.batchSamplerParam(ii));
                sampledBBoxes = cat(1, sampledBBoxes, samples);
            end
            if size(sampledBBoxes, 1) > 0
                % Randomly pick a sampled bbox and crop the img.
                pickId  = floor(rng_uniform(1, 1, size(sampledBBoxes, 1)));
                [sampledImg, BBoxes] = obj.cropImage(img, BBoxes, sampledBBoxes(pickId, :));
            else
                sampledImg = img;
            end
       end
   end
   
   methods(Access = private)
       function sampledBBoxes = generateSamples(obj, sourceBBox, objectBBoxes, batchSampler)
           found = 0;
           sampledBBoxes = [];
           for ii = 1 : batchSampler.max_trials
               if found >= batchSampler.max_sample
                    break;
               end
               if isempty(batchSampler.sampler)
                   batchSampler.sampler = struct();
               end
               sampledBBox = obj.sampleBBox(batchSampler.sampler);
               sampledBBox = BBoxUtil.locateBBox(sourceBBox, sampledBBox);
               if obj.satisfySampleConstraint(sampledBBox, objectBBoxes, ...
                                              batchSampler.sample_constraint)
                   found = found + 1;
                   sampledBBoxes    = cat(1, sampledBBoxes, sampledBBox);
               end
           end
       end
       
       function sampled_bbox = sampleBBox(obj, sampler)
           assert(sampler.min_scale <= sampler.max_scale, 'max_scale must be >= min_scale');
           assert(sampler.min_scale > 0, 'min scale must be > 0');
           assert(sampler.max_scale <= 1, 'max scale must be <= 1');
           
           % Get random scale.
           scale = rng_uniform(1, sampler.min_scale, sampler.max_scale);
           
           % Get random aspect ratio.
           assert(sampler.max_aspect_ratio >= sampler.min_aspect_ratio, ...
                        'max aspect ratio must be >= min aspect ratio');
           assert(sampler.min_aspect_ratio > 0, 'min aspect ratio must be > 0');
           minAspectRatio   = max(sampler.min_aspect_ratio, scale^2);
           maxAspectRatio   = min(sampler.max_aspect_ratio, 1/scale^2);
           aspectRatio      = rng_uniform(1, minAspectRatio, maxAspectRatio);
           
           % Figure out bbox dimension.
           bboxWidth        = scale * sqrt(aspectRatio);
           bboxHeight       = scale / sqrt(aspectRatio);
           
           % Figure out top left coordinates.
           wOff             = rng_uniform(1, 0, 1 - bboxWidth);
           hOff             = rng_uniform(1, 0, 1 - bboxHeight);
           sampled_bbox     = [wOff, hOff, wOff + bboxWidth, hOff + bboxHeight];
       end
       
       function isSatis = satisfySampleConstraint(obj, sampledBBox, objectBBoxes, ...
                                                       sampleConstraint)
           isSatis = false;
           if isempty(sampleConstraint)
                isSatis = true;
                return ;
           end
           iou  = BBoxUtil.jaccardOverlap(objectBBoxes, sampledBBox);
           isSatis = false;
           for ii = 1 : size(objectBBoxes, 1)
                if isfield(sampleConstraint, 'min_jaccard_overlap') && ...
                    iou(ii) < sampleConstraint.min_jaccard_overlap
                    continue;
                end
                
                if isfield(sampleConstraint, 'max_jaccard_overlap') && ...
                   iou(ii) > sampleConstraint.max_jaccard_overlap 
                   continue;
                end
                isSatis = true;
                break;
           end
       end
       
       
       function [cropedImg, bboxes] = cropImage(obj, img, bboxes, bbox)
            [height, width, ~] = size(img);
            rect = BBoxUtil.xy2wh(BBoxUtil.recoverBBox(height, width, bbox));
            cropedImg   = imcrop(img, rect);
            trans       = TransformData();
            cropBBox    = BBoxUtil.clipBBox(bbox, [0, 1], [0, 1]);
            bboxes      = trans.transformAnnotation(img, false, cropBBox, false, bboxes);
       end
   end
   
end