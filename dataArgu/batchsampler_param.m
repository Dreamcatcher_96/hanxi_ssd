% Sample a batch of bboxes with provided constraints.
function [batch_sampler] = batchsampler_param()
    batch_sampler(1).max_sample             = 1;
    batch_sampler(1).max_trials             = 50;
    batch_sampler(1).sampler.min_scale      = 0.3;
    batch_sampler(1).sampler.max_scale      = 1.0;
    batch_sampler(1).sampler.min_aspect_ratio               = 0.5;
    batch_sampler(1).sampler.max_aspect_ratio               = 2.0;
    batch_sampler(1).sample_constraint.min_jaccard_overlap  = 0.1;


    batch_sampler(2).max_sample             = 1;
    batch_sampler(2).max_trials             = 50;
    batch_sampler(2).sampler.min_scale      = 0.3;
    batch_sampler(2).sampler.max_scale      = 1.0;
    batch_sampler(2).sampler.min_aspect_ratio               = 0.5;
    batch_sampler(2).sampler.max_aspect_ratio               = 2.0;
    batch_sampler(2).sample_constraint.min_jaccard_overlap  = 0.3;


    batch_sampler(3).max_sample             = 1;
    batch_sampler(3).max_trials             = 50;
    batch_sampler(3).sampler.min_scale      = 0.3;
    batch_sampler(3).sampler.max_scale      = 1.0;
    batch_sampler(3).sampler.min_aspect_ratio               = 0.5;
    batch_sampler(3).sampler.max_aspect_ratio               = 2.0;
    batch_sampler(3).sample_constraint.min_jaccard_overlap  = 0.5;


    batch_sampler(4).max_sample             = 1;
    batch_sampler(4).max_trials             = 50;
    batch_sampler(4).sampler.min_scale      = 0.3;
    batch_sampler(4).sampler.max_scale      = 1.0;
    batch_sampler(4).sampler.min_aspect_ratio               = 0.5;
    batch_sampler(4).sampler.max_aspect_ratio               = 2.0;
    batch_sampler(4).sample_constraint.min_jaccard_overlap  = 0.7;


    batch_sampler(5).max_sample             = 1;
    batch_sampler(5).max_trials             = 50;
    batch_sampler(5).sampler.min_scale      = 0.3;
    batch_sampler(5).sampler.max_scale      = 1.0;
    batch_sampler(5).sampler.min_aspect_ratio               = 0.5;
    batch_sampler(5).sampler.max_aspect_ratio               = 2.0;
    batch_sampler(5).sample_constraint.min_jaccard_overlap  = 0.9;   


    batch_sampler(6).max_sample             = 1;
    batch_sampler(6).max_trials             = 50;
    batch_sampler(6).sampler.min_scale      = 0.3;
    batch_sampler(6).sampler.max_scale      = 1.0;
    batch_sampler(6).sampler.min_aspect_ratio               = 0.5;
    batch_sampler(6).sampler.max_aspect_ratio               = 2.0;
    batch_sampler(6).sample_constraint.max_jaccard_overlap  = 1.0;  
    
	batch_sampler(7).max_sample             = 1;
    batch_sampler(7).max_trials             = 1;
    batch_sampler(7).sampler.min_scale      = 1;
    batch_sampler(7).sampler.max_scale      = 1;  
    batch_sampler(7).sampler.min_aspect_ratio               = 1;
    batch_sampler(7).sampler.max_aspect_ratio               = 1;  
    
end