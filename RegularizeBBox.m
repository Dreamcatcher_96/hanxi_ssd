% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期二 10/01/2017 17:08.
% Last Revision: 星期二 10/01/2017 17:08.
%qt

function bbox = RegularizeBBox(bbox, im_siz)

    bbox(1, :) = max(1, min(bbox(1, :), im_siz(2)));
    bbox(2, :) = max(1, min(bbox(2, :), im_siz(1)));
    bbox(3, :) = max(1, min(bbox(3, :), im_siz(2)));
    bbox(4, :) = max(1, min(bbox(4, :), im_siz(1)));

end



