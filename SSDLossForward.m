% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期二 24/01/2017 23:12.
% Last Revision: 星期二 24/01/2017 23:12.
%qt

function [loc_loss, conf_loss] = SSDLossForward(pos_dbox_all, ...
            pos_gbox_all, all_loc_preds, all_obj_conf, dbox, net_input_label, ...
                                    neg_dbox_all, num_pos, num_neg, net_opt)

    loc_loss.loss = 0;
    loc_pred_data = [];
    loc_gt_data = [];
    if num_pos >= 1
        for i_im = 1 : net_opt.batch_size
            pos_dbox_idx = pos_dbox_all{i_im};
            matched_gt_idx = pos_gbox_all{i_im};
            if isempty(pos_dbox_idx), continue; end

            % the predictions
            loc_pred = all_loc_preds(:, :, i_im);
            loc_pred_data = cat(1, loc_pred_data, ...
                                loc_pred(:, pos_dbox_idx)');

            % ground truth
            loc_gt = net_input_label{i_im};
            loc_gt = loc_gt(matched_gt_idx, :);

            % real ground truth
            loc_gt_encode = EncodeBBox(...
                    dbox(:, pos_dbox_idx), loc_gt', net_opt)';
            loc_gt_data = cat(1, loc_gt_data, loc_gt_encode);
        end
        % loc forwards
        loc_loss = SmoothL1Forward(loc_pred_data, loc_gt_data);
    end

    num_conf = num_pos + num_neg;
    conf_loss.loss = 0;
    if num_conf > 1
        conf_gt_data = [];
        obj_conf_data = [];

        for i_im = 1 : net_opt.batch_size
            pos_dbox_idx = pos_dbox_all{i_im};
            matched_gt_idx = pos_gbox_all{i_im};
            conf_gt = net_input_label{i_im}(:, 5);

            cur_conf_data = all_obj_conf(:, :, i_im);
            if isempty(pos_dbox_idx)
               continue;
            end

            conf_gt_data = cat(1, conf_gt_data, ...
                                        conf_gt(matched_gt_idx));
            obj_conf_data = cat(2, obj_conf_data, ...
                                        cur_conf_data(:, pos_dbox_idx));

            neg_idx = neg_dbox_all{i_im};
            conf_gt_data = cat(1, conf_gt_data, ...
                repmat(net_opt.background_id, numel(neg_idx), 1));
            obj_conf_data = cat(2, obj_conf_data, ...
                                cur_conf_data(:, neg_idx));
        end
        conf_loss = SoftMaxLossForward(obj_conf_data, conf_gt_data);
    end

end


