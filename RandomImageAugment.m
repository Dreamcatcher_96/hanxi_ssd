% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期日 19/03/2017 01:35.
% Last Revision: 星期日 19/03/2017 01:35.

function [im_aug, obj_bbox_aug] = RandomImageAugment(im, obj_bbox)

    obj_bbox_aug = obj_bbox;
    im_aug = im;

    % color or brightness distortion
    if rand > 0.5
        trans = TransformData();
        im_aug = trans.applyDistort(im);
    end

    % flipping
    if rand > 0.5
        im_aug = flip(im_aug, 2);
        obj_bbox_aug(3, :) = size(im, 2) - obj_bbox(1, :) + 1;
        obj_bbox_aug(1, :) = size(im, 2) - obj_bbox(3, :) + 1;
    end

    % add image margin
    margin_ratio = 0.1 + rand * 0.2;
    [im_aug, offset] = ImageAddMargin(im_aug, margin_ratio);
    obj_bbox_aug([1, 3], :) = obj_bbox_aug([1, 3], :) + offset(2);
    obj_bbox_aug([2, 4], :) = obj_bbox_aug([2, 4], :) + offset(1);

%     % for debugging
%     figure(1);
%     subplot(1, 2, 1);imagesc(im);axis equal;hold on;
%     for i_bbox = 1 : size(obj_bbox, 2)
%         PlotBlockSimple(obj_bbox(:, i_bbox), '-', 'g');
%     end
%     subplot(1, 2, 2);imagesc(uint8(im_aug));axis equal;hold on;
%     for i_bbox = 1 : size(obj_bbox_aug, 2)
%         PlotBlockSimple(obj_bbox_aug(:, i_bbox), '-', 'g');
%     end
%     pause(0.1);clf;

end



