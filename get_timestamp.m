function timeStamp = get_timestamp()
    sTime = [1970, 1, 1, 0, 0, 0];
    eTime = clock();
    timeStamp = floor((datenum(eTime) - datenum(sTime))) * 86400 ...
                    + eTime(4) * 3600 + eTime(5) * 60 + floor(eTime(6));
    
end