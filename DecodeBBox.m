% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期二 14/03/2017 09:50.
% Last Revision: 星期二 14/03/2017 09:50.

function bbox = DecodeBBox(dbox, bbox_code, net_opt)

%     % dbox  bbox  --> xmin; ymin; xmax; ymax;
%     dbox(5, :) = dbox(3, :) - dbox(1, :); % width
%     dbox(6, :) = dbox(4, :) - dbox(2, :); % height
%     dbox(7, :) = (dbox(1, :) + dbox(3, :)) ./ 2; % center x
%     dbox(8, :) = (dbox(2, :) + dbox(4, :)) ./ 2; % center y

    bbox_center_x = dbox(7, :) + net_opt.code_lambda_1 .* bbox_code(1, :) .* dbox(5, :);
    bbox_center_y = dbox(8, :) + net_opt.code_lambda_1 .* bbox_code(2, :) .* dbox(6, :);
    bbox_width = exp(net_opt.code_lambda_2 .* bbox_code(3, :)) .* dbox(5, :);
    bbox_height = exp(net_opt.code_lambda_2 .* bbox_code(4, :)) .* dbox(6, :);

    bbox = zeros(size(bbox_code));
    bbox(1, :) = bbox_center_x - 0.5 * bbox_width;
    bbox(2, :) = bbox_center_y - 0.5 * bbox_height;
    bbox(3, :) = bbox_center_x + 0.5 * bbox_width;
    bbox(4, :) = bbox_center_y + 0.5 * bbox_height;

end


