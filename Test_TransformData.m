% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期六 18/03/2017 23:43.
% Last Revision: 星期六 18/03/2017 23:43.

function Test_TransformData()

    clc;
    clear;
    close all;
    dbstop if error;

    addpath('./dataArgu/');
    dataset_name = 'voc0712';
    data_opt = DatasetOption(dataset_name);
    dataset = GetDataset(data_opt);
    dataset = dataset.data_train;
    num_trn_data = numel(dataset);

    trans = TransformData();
    for i_im = 1 : num_trn_data
        im_name = [data_opt.image_root, '/', dataset{i_im}.filename];
        im = imread(im_name);
        distortedImg = trans.applyDistort(im);
        [im_large, offset] = ImageAddMargin(distortedImg, 0.2);
%         obj = dataset{i_im}.object;
%         [expandedImg, anno] = trans.expandImage(distortedImg, anno);
        figure(1);subplot(1, 2, 1);
        imagesc(uint8(im_large));axis equal;
        figure(1);subplot(1, 2, 2);
        imagesc(im);axis equal;
        pause(0.1);clf;
    end

%     nTest   = length(imdb.image_ids);
%     nIdx    = randperm(length(imdb.image_ids), nTest);
%     count   = 0;
%     timer   = zeros(1, nTest);
%     for ii = 1 : nTest

%         idx     = nIdx(ii);
%         if strfind(imdb.image_ids{idx}, 'flip')
%            continue;
%         end
%         img     = imread(imdb.image_at(idx));
%         anno    = roidb.rois(idx);

%         subplot(1, 2, 1);
%         annoedImg = insertObjectAnnotation(img, 'rectangle', ...
%                                 BBoxUtil.xy2wh(anno.boxes), imdb.classes(anno.class));
%         imshow(annoedImg);

%         subplot(1, 2, 2);
%         tm          = tic();
%         count       = count + 1;
%         siz         = imdb.sizes(idx, :);
%         anno.boxes = BBoxUtil.normalizeBBox(siz(1), siz(2), anno.boxes);
%         distortedImg            = trans.applyDistort(img);
%         [expandedImg, anno]     = trans.expandImage(distortedImg, anno);
%         sampler                 = BatchSampler();
%         [sampledImg, anno]      = sampler.generateBatchSamples(expandedImg, anno);
%         [resizedImg, anno]      = trans.applyResize(sampledImg, anno);
%         timer(ii) = toc(tm);
%     %     transedImg = trans.applyResize(img);

%     %     [expandedImg, anno] = trans.expandImage(img, anno);
%     %     [height, width, ~] = size(expandedImg);
%     %     anno.boxes = BBoxUtil.recoverBBox(height, width, anno.boxes);
%     %     expandedImg = insertObjectAnnotation(expandedImg, 'rectangle', ...
%     %                             xy2wh(anno.boxes), imdb.classes(anno.class));
%     %     imshow(expandedImg);
%     %     pause(5);
%     %     [resizedImg, anno] = trans.applyResize(img, anno);
%         [height, width, ~] = size(resizedImg);
%         anno = BBoxUtil.delInvalidBBoxes(anno);
%         if ~isempty(anno.boxes)
%             anno.boxes = BBoxUtil.recoverBBox(height, width, anno.boxes);
%             resizedImg = insertObjectAnnotation(resizedImg, 'rectangle', ...
%                                     BBoxUtil.xy2wh(anno.boxes), imdb.classes(anno.class));
%         end
%         imshow(resizedImg);
%         pause(0.1);
%     end

%     timeCunsum = sum(timer) / count;

end



